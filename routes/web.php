<?php

use Illuminate\Support\Facades\Route;
use App\Http\controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/admin', [HomeController::class, 'admin']);
Route::get('/table', [HomeController::class, 'table']);
Route::post('/delete/contacts', [HomeController::class, 'delete']);
Route::post('/table', [HomeController::class, 'save']);
Route::put('/contact/{id}', [HomeController::class, 'update']);
