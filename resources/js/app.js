import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/inertia-vue3'
import Layout from './Layout.vue'

createInertiaApp({
  resolve: name => {
    const component = require(`./Pages/${name}`)
    component.default.layout = Layout;
    return component;
    },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .mount(el)
  },
})