<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;
use App\Models\Contact;

class HomeController extends Controller
{
    //
    public function index(){
        return Inertia::render('Bonjour', [
            'name' => 'daniel'
        ]);
    }

    public function admin(){
        return Inertia::render('Admin', [
            'prenom' => Str::random(10),
            'nom' => Str::random(10),
        ]);
    }

    public function table(Request $request){
        $query = Contact::query();
        if($request->query->has('search')) {
            $query->where('name', 'LIKE', '%'.$request->query->get('search').'%')
                ->orWhere('email', 'LIKE', '%'.$request->query->get('search').'%')
                ->orWhere('city', 'LIKE', '%'.$request->query->get('search').'%');
        }
        if($request->query->has('field') and $request->query->has('direction') ) {
            $query->orderBy($request->query->get('field'), $request->query->get('direction'));
        }
        $nbPerPage = $request->query->has('perPage') ? $request->query->get('perPage') : 10;
        $contacts = $query->paginate($nbPerPage);
        return Inertia::render('DataTable', [
            'table' => $contacts,
            'filters' => $request->all(['field','direction','search','perPage'])
        ]);
    }

    public function delete(Request $request){
        Contact::destroy($request->get("data"));
        return redirect()->back()->with('success message');
    }

    public function save(Request $request){
        $request->validate([
            'name' => ['required'],
            'email' => ['required'],
        ]);
        Contact::create($request->all());
        return redirect()->back()->with('contact create !!!');
    }

    public function update(Request $request){
        $contact = Contact::findOrFail($request->id);
        $contact->update([
            "name" => $request->name,
            "email" => $request->email,
            "city" => $request->city,
            "phoneNumber" => $request->phoneNumber,
            "postalCode" => $request->postalCode,
        ]);
        $contact->save();
        return redirect()->back()->with('contact update !!!');
    }
}
